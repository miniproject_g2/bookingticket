import java.util.Scanner;

public class BookingSystem {
    private String[] movies = { "Movie A", "Movie B", "Movie C" };
    private Scanner scanner;
    private String[][] seats = { { "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8" },
            { "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8" },
            { "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8" },
            { "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8" } };
    private static String[] showTiming = { "11:00", "15:00", "19:00" };
    private int seatPrice = 160;
    private int totalPrice = 0;

    public BookingSystem() {
        scanner = new Scanner(System.in);
    }

    public void displayMovies() {
        System.out.println("Available Movies:");
        for (int i = 0; i < movies.length; i++) {
            System.out.println((i + 1) + ". " + movies[i]);
        }
    }

    public void displayShowtime() {
        System.out.println("Available Showtime:");
        for (int i = 0; i < showTiming.length; i++) {
            System.out.println((i + 1) + ". " + showTiming[i]);
        }
    }

    public void bookingMovie() {
        System.out.println("=============================");
        System.out.print("Please select movie you want to watch: ");
        int selectMovie = scanner.nextInt();
        for (int i = 0; i < movies.length; i++) {
            if (selectMovie == i) {
                System.out.println("You have select: " + movies[i - 1]);
            }
        }
    }

    public void bookingTime() {
        System.out.println("=============================");
        System.out.print("Please select time you want to watch: ");
        int selectTime = scanner.nextInt();
        for (int i = 0; i < showTiming.length; i++) {
            if (selectTime == i) {
                System.out.println("You have select: " + showTiming[i - 1]);
            }
        }
    }

    public void cancelBooking() {
        System.out.println("Are you sure you want to cancel this booking?");
    }

    public void run() {
        while (true) {
            displayMovies();
            bookingMovie();
            displayShowtime();
            bookingTime();
            messageSelectSeat();
            showSeats();
            bookingSeat();
            showSeats();
            showTotalPrice();
            break;
        }
    }

    private void showTotalPrice() {
        System.out.println("=============================");
        System.out.println("Your total price is "+totalPrice+" Baht");
    }

    public void messageSelectSeat() {
        System.out.println("=============================");
        System.out.println("Please select seat");
    }

    public void showSeats() {
        System.out.println("-------------------------------");
        System.out.println("|         Movie Screen        |");
        System.out.println("-------------------------------");
        for (int row = 0; row < seats.length; row++) {
            for (int column = 0; column < seats[row].length; column++) {
                System.out.print(seats[row][column] + "  ");
            }
            System.out.println();
        }
    }

    public void bookingSeat() {
        System.out.println("=============================");
        System.out.print("Please type number of seat you want to book: ");
        int numSeat = scanner.nextInt();
        String[] bookSeat = new String[numSeat];
        for (int num = 1; num <= numSeat; num++) {
            System.out.print("Please type the seat number you want to book no." + (num) + ": ");
            String selectSeat = scanner.next();
            if (isSeatBooked(selectSeat)==true) {
                bookSeat[num - 1] = selectSeat;
                totalPrice = totalPrice + seatPrice;
            } else {
                System.out.println("Sorry, that seat is already taken.");
                num--;
            }
        }
    }

    public boolean isSeatBooked(String seat) {
        for (int row = 0; row < seats.length; row++) {
            for (int col = 0; col < seats[row].length; col++) {
                if (seats[row][col].toString().equals(seat)) {
                    seats[row][col] = "-";
                    return true;
                }
            }
        }
        return false;
    }
}
