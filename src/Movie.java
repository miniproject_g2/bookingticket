public class Movie {
    private String name;
    private static String[] showTiming = {"11:00","15:00","19:00"};

    public Movie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String[] getShowTiming() {
        return showTiming;
    }
}
