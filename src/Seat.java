class Seat {
    private int row;
    private int column;
    boolean isTaken;
    private String[][] seats = {{"A1","A2","A3","A4","A5","A6","A7","A8"},
                            {"B1","B2","B3","B4","B5","B6","B7","B8"},
                            {"C1","C2","C3","C4","C5","C6","C7","C8"},
                            {"D1","D2","D3","D4","D5","D6","D7","D8"}};

    public Seat(int row, int column) {
        this.row = row;
        this.column = column;
        this.isTaken = false;
    }

    public String[][] getSeat() {
        return seats;
    }

    public String toString() {
        return "Row " + row + ", Seat " + column + " " + (isTaken ? "(Taken)" : "(Available)");
    }
}
