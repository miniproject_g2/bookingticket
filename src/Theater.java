class Theater {
    private int rows;
    private int columns;
    private Seat seat;
    private String[][] seats;

    public Theater() {
        this.seats = this.seat.getSeat();        
    }

    public void showSeats() {
        for (int row = 0; row < seats.length; row++) {
            for (int column = 0; column < seats[row].length; column++) {
                System.out.print(seats[row][column] + "  ");
            }
            System.out.println();
        }
    }

    public void takeSeat(int row, int column) {
        if (row >= 1 && row <= rows && column >= 1 && column <= columns) {
            //Seat seat = seats[row - 1][column - 1];
            if (!seat.isTaken) {
                seat.isTaken = true;
                System.out.println("Seat at Row " + row + ", Seat " + column + " has been taken.");
            } else {
                System.out.println("Sorry, that seat is already taken.");
            }
        } else {
            System.out.println("Invalid row or column.");
        }
    }
}